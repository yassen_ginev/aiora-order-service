<?php
namespace Krasimird\AioraOrderService\Http\Responses;

class OrderResponse
{
    public $msg;
    public $data;
    public $statusCode;

    /**
     * OrderResponse constructor.
     * @param $msg
     * @param $data
     * @param $statusCode
     */
    public function __construct($msg, $statusCode, $data )
    {
        $this->msg = $msg;
        $this->statusCode = $statusCode;
        $this->data = $data;
    }


    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param mixed $msg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }



}
